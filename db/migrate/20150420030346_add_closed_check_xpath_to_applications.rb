class AddClosedCheckXpathToApplications < ActiveRecord::Migration
  def change
    add_column :applications, :closed_check_xpath, :string, null: false
  end
end
