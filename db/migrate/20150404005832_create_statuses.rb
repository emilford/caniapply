class CreateStatuses < ActiveRecord::Migration
  def change
    create_table :statuses do |t|
      t.integer :current, null: false, default: 0
      t.timestamps
    end
  end
end
