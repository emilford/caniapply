class AddClosedCheckUrlToApplications < ActiveRecord::Migration
  def change
    add_column :applications, :closed_check_url, :string, null: false
  end
end
