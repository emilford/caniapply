class AddApplicationIdToStatuses < ActiveRecord::Migration
  def change
    add_column :statuses, :application_id, :integer
    add_index :statuses, :application_id
  end
end
