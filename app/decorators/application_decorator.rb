class ApplicationDecorator < BaseDecorator
  def current_status
    latest_status.current
  end

  def last_updated_at
    I18n.localize(latest_status.created_at)
  end

  private

  def latest_status
    statuses.latest
  end
end
