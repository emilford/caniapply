class BaseDecorator < SimpleDelegator
  def self.decorate(instances)
    instances.map { |instance| new(instance) }
  end

  def initialize(instance)
    super(instance)
  end
end
