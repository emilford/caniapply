class Notifier < ActionMailer::Base
  default from: "no-reply@caniapply.com"

  def applications_open(application)
    mail(
      to: "ericmilford@gmail.com",
      subject: "#{application.name} Applications Open!"
    )
  end
end
