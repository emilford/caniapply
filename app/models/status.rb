class Status < ActiveRecord::Base
  belongs_to :application

  enum current: { closed: 0, open: 1 }

  def self.latest
    order(created_at: :desc).first || NullStatus.new
  end

  def self.previous
    order(created_at: :desc).offset(1).first || NullStatus.new
  end
end
