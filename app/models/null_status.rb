class NullStatus
  def closed?
    true
  end

  def open?
    false
  end

  def current
    "closed"
  end

  def created_at
    Time.current
  end
end
