class ApplicationsController < ApplicationController
  def index
    @applications = ApplicationDecorator.decorate(Application.all)
  end
end
