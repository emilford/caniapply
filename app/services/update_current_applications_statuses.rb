class UpdateCurrentApplicationsStatuses
  def self.run
    Application.find_each do |application|
      new(application).run
    end
  end

  def initialize(application)
    @application = application
  end

  def run
    if applications_closed?
      create_closed_status
    else
      create_open_status
      SendEmailNotifications.run(application)
    end
  end

  private

  attr_reader :application

  def applications_closed?
    page_body.has_xpath?(application.closed_check_xpath)
  end

  def page_body
    response = Faraday.get(application.closed_check_url)
    Capybara::Node::Simple.new(response.body)
  end

  def create_open_status
    application.statuses.open.create!
  end

  def create_closed_status
    application.statuses.closed.create!
  end
end
