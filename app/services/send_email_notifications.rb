class SendEmailNotifications
  def self.run(application)
    new(application).run
  end

  def initialize(application)
    @application = application
  end

  def run
    return unless applications_newly_opened?
    Notifier.applications_open(application).deliver_now
  end

  private

  attr_reader :application

  def applications_newly_opened?
    return false if latest_status.closed?
    return false if previous_status.open?
    true
  end

  def latest_status
    application.statuses.latest
  end

  def previous_status
    application.statuses.previous
  end
end
