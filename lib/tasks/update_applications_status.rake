namespace :status do
  desc "fetches latest application statuses"
  task update: :environment do
    UpdateCurrentApplicationsStatuses.run
  end
end
