FactoryGirl.define do
  factory :application do
    sequence(:name) { |n| "Application #{n}" }
    closed_check_url "http://www.apprentice.io/developer"
    closed_check_xpath "//a[@href='#disabled_explanation']"

    trait :closed do
      before(:create) do |application|
        application.statuses.closed.build
      end
    end

    trait :decorated do
      initialize_with do
        ApplicationDecorator.new(new)
      end
    end
  end

  factory :status do
    trait :closed do
      current :closed
    end

    trait :open do
      current :open
    end
  end
end
