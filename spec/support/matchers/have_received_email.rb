RSpec::Matchers.define :have_received_email do
  match do |email|
    deliveries_for(email).present?
  end

  failure_message do |email|
    "expected #{email} to have received email with #{options}"
  end

  failure_message_when_negated do |email|
    "expected #{email} not to have received email with #{options}"
  end

  chain :with_subject do |subject|
    options.merge!(with_subject: subject)
  end

  def options
    @options ||= {}
  end

  def deliveries_for(email)
    deliveries = ActionMailer::Base.deliveries.select do |delivery|
      delivery.destinations.include?(email)
    end

    return deliveries unless options[:with_subject]

    deliveries.find do |delivery|
      delivery.subject =~ Regexp.new(options[:with_subject])
    end
  end
end
