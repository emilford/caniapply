module Dom
  class Application < Domino
    selector "[data-application]"

    attribute :name, "[data-name]"
    attribute :status, "[data-status]"

    def closed?
      status[/closed/i]
    end

    def open?
      status[/open/i]
    end
  end
end
