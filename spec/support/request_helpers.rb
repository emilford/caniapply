module RequestHelpers
  def stub_request_applications_closed
    stub_request(:get, apprentice_io_developer_url)
      .and_return(applications_closed_response)
  end

  def stub_request_applications_open
    stub_request(:get, apprentice_io_developer_url)
      .and_return(applications_open_response)
  end

  def simulate_update_applications_closed
    stub_request_applications_closed
    UpdateCurrentApplicationsStatuses.run
  end

  def simulate_update_applications_open
    stub_request_applications_open
    UpdateCurrentApplicationsStatuses.run
  end

  private

  def applications_closed_response
    File.open("spec/support/fixtures/applications-closed-response.txt")
  end

  def applications_open_response
    File.open("spec/support/fixtures/applications-open-response.txt")
  end

  def apprentice_io_developer_url
    "http://www.apprentice.io/developer"
  end
end

RSpec.configure do |config|
  config.include(RequestHelpers)
end
