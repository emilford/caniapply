require "rails_helper"

RSpec.describe "applications/index" do
  it "displays a timestamp for the last application status update" do
    application = create(:application, :decorated)
    create(
      :status,
      application_id: application.id,
      created_at: Time.zone.parse("2015-04-03 00:00:00")
    )

    assign(:applications, [application])

    render

    expect(rendered).to have_selector(
      "[data-timestamp]", text: "April 03, 2015 at 12AM"
    )
  end
end
