require "rails_helper"

RSpec.describe SendEmailNotifications do
  it "sends email when applications go from closed to open" do
    application = create(:application)
    create(:status, :closed, application: application)
    create(:status, :open, application: application)

    allow(Notifier).to receive(:applications_open) { double(deliver_now: true) }

    SendEmailNotifications.run(application)

    expect(Notifier).to have_received(:applications_open)
  end

  it "does not send email when applications are still closed" do
    application = create(:application)
    create(:status, :closed, application: application)

    allow(Notifier).to receive(:applications_open) { double(deliver_now: true) }

    SendEmailNotifications.run(application)

    expect(Notifier).not_to have_received(:applications_open)
  end

  it "does not send email when applications were already open" do
    application = create(:application)
    create(:status, :open, application: application)
    create(:status, :open, application: application)

    allow(Notifier).to receive(:applications_open) { double(deliver_now: true) }

    SendEmailNotifications.run(application)

    expect(Notifier).not_to have_received(:applications_open)
  end
end
