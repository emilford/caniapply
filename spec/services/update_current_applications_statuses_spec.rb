require "rails_helper"

RSpec.describe UpdateCurrentApplicationsStatuses do
  it "runs a status update for each application" do
    application_1 = create(:application)
    application_2 = create(:application)
    stub_request_applications_closed

    UpdateCurrentApplicationsStatuses.run

    expect(application_1.statuses.closed.count).to eq(1)
    expect(application_2.statuses.closed.count).to eq(1)
  end

  it "creates a closed status entry when applications are closed" do
    application = create(:application)
    stub_request_applications_closed

    UpdateCurrentApplicationsStatuses.run

    expect(application.statuses.closed.count).to eq(1)
    expect(application.statuses.open.count).to eq(0)
  end

  context "when applications are open" do
    it "creates an open status entry" do
      application = create(:application)
      stub_request_applications_open

      UpdateCurrentApplicationsStatuses.run

      expect(application.statuses.closed.count).to eq(0)
      expect(application.statuses.open.count).to eq(1)
    end

    it "triggers email notifications" do
      create(:application)
      stub_request_applications_open
      allow(SendEmailNotifications).to receive(:run)

      UpdateCurrentApplicationsStatuses.run

      expect(SendEmailNotifications).to have_received(:run)
    end
  end
end
