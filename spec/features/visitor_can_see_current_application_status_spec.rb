require "rails_helper"

RSpec.feature "visitor can view current application status" do
  scenario "sees open when applications are open, otherwise closed" do
    create(:application, name: "Apprentice.io Developer")
    create(:application, name: "Full-Time Developer")

    visit root_path
    expect(apprentice_io_developer_applications).to be_closed
    expect(full_time_developer_applications).to be_closed

    simulate_update_applications_closed

    visit root_path
    expect(apprentice_io_developer_applications).to be_closed
    expect(full_time_developer_applications).to be_closed

    simulate_update_applications_open

    visit root_path
    expect(apprentice_io_developer_applications).to be_open
    expect(full_time_developer_applications).to be_open
  end

  def apprentice_io_developer_applications
    Dom::Application.find_by(name: "Apprentice.io Developer")
  end

  def full_time_developer_applications
    Dom::Application.find_by(name: "Full-Time Developer")
  end
end
