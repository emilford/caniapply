require "rails_helper"

RSpec.feature "recipients recive email notifications" do
  scenario "receive when application status changes from closed to open" do
    create(:application, :closed, name: "Apprentice.io Developer")

    simulate_update_applications_open

    expect("ericmilford@gmail.com").to have_received_email
      .with_subject("Apprentice.io Developer Applications Open!")
  end
end
