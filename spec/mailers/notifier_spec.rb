require "rails_helper"

RSpec.describe Notifier do
  describe "#applications_open" do
    let(:application) do
      build(:application, name: "Apprentice.io Developer")
    end

    let(:email) do
      Notifier.applications_open(application)
    end

    it "sends from the correct email address" do
      expect(email).to deliver_from("no-reply@caniapply.com")
    end

    it "sends to the correct recipient(s)" do
      expect(email).to deliver_to("ericmilford@gmail.com")
    end

    it "has the correct subject" do
      expect(email)
        .to have_subject("Apprentice.io Developer Applications Open!")
    end
  end
end
