require "rails_helper"

RSpec.describe BaseDecorator do
  describe ".decorate" do
    it "returns a collection of decorated instances" do
      instance_1 = double(:instance)
      instance_2 = double(:instance)

      instances = BaseDecorator.decorate([instance_1, instance_2])

      expect(instances[0]).to be_a(BaseDecorator)
      expect(instances[1]).to be_a(BaseDecorator)
    end
  end
end
