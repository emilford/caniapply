require "rails_helper"

RSpec.describe ApplicationDecorator do
  describe "#current_status" do
    it "returns the state of the latest status" do
      application = create(:application, :decorated)
      create(:status, :open, application_id: application.id)
      create(:status, :closed, application_id: application.id)

      expect(application.current_status).to eq("closed")
    end
  end

  describe "#last_updated_at" do
    it "returns the formatted created at date of the latest status" do
      application = create(:application, :decorated)
      create(
        :status,
        application_id: application.id,
        created_at: Time.zone.parse("2015-04-02 11:00:00")
      )
      create(
        :status,
        application_id: application.id,
        created_at: Time.zone.parse("2015-04-03 11:00:00")
      )

      expect(application.last_updated_at).to eq("April 03, 2015 at 11AM")
    end
  end
end
