require "rails_helper"
require "rake"

RSpec.describe "update_applications_status.rake" do
  before do
    load "tasks/update_applications_status.rake"
    Rake::Task.define_task(:environment)
  end

  it "triggers the UpdateCurrentApplicationsStatuses class" do
    allow(UpdateCurrentApplicationsStatuses).to receive(:run)

    Rake::Task["status:update"].invoke

    expect(UpdateCurrentApplicationsStatuses).to have_received(:run)
  end
end
