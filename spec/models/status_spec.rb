require "rails_helper"

RSpec.describe Status do
  it { is_expected.to belong_to(:application) }

  describe ".latest" do
    it "returns the most recently created status" do
      status_1 = create(:status, :closed, created_at: 1.days.ago)
      _status_2 = create(:status, :open, created_at: 2.days.ago)

      latest = Status.latest

      expect(latest).to eq(status_1)
    end

    it "returns a NullStatus object when no statuses exist" do
      latest = Status.latest

      expect(latest).to be_an_instance_of(NullStatus)
    end
  end

  describe ".previous" do
    it "returns the next to most recently created status" do
      _status_1 = create(:status, :closed, created_at: 1.days.ago)
      status_2 = create(:status, :closed, created_at: 2.days.ago)

      previous = Status.previous

      expect(previous).to eq(status_2)
    end

    it "returns a NullStatus object when no statuses exist" do
      previous = Status.previous

      expect(previous).to be_an_instance_of(NullStatus)
    end
  end
end
