require "rails_helper"

RSpec.describe Application do
  it { is_expected.to have_many(:statuses).dependent(:destroy) }
end
