require "rails_helper"

RSpec.describe NullStatus do
  subject(:null_status) do
    NullStatus.new
  end

  describe "#closed?" do
    it "always returns true" do
      closed = null_status.closed?

      expect(closed).to eq(true)
    end
  end

  describe "#open?" do
    it "always returns false" do
      open = null_status.open?

      expect(open).to be(false)
    end
  end

  describe "#current" do
    it "always returns closed" do
      current = null_status.current

      expect(current).to eq("closed")
    end
  end

  describe "#created_at" do
    it "always returns the current time" do
      created_at = null_status.created_at

      expect(created_at).to be_within(0.1).of(Time.current)
    end
  end
end
